# ---- Base Node ----
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost1.55-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/urocoin/uro.git /opt/urocoin && \
    cd /opt/urocoin/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:14.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost1.55-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r urocoin && useradd -r -m -g urocoin urocoin
RUN mkdir /data
RUN chown urocoin:urocoin /data
COPY --from=build /opt/urocoin/src/Urod /usr/local/bin/
USER urocoin
VOLUME /data
EXPOSE 36348
CMD ["/usr/local/bin/Urod", "-datadir=/data", "-conf=/data/Uro.conf", "-server", "-txindex", "-printtoconsole"]